
-- Script SQL de création des tables :

-- rum,rum_acte,acte,rum_diagnostic,diagnostic

-----------------------------------------------
-----------------------------------------------


----- Création de la table rum ------
-----------------------------------------

CREATE TABLE rum(
--
id_rum SERIAL NOT NULL,
id_sejour INT NOT NULL,
date_entree DATE NOT NULL,
date_sortie DATE NOT NULL,
mode_admission VARCHAR(50) NOT NULL,
mode_sortie VARCHAR(50) NOT NULL,
--
PRIMARY KEY (id_rum),
FOREIGN KEY (id_sejour) REFERENCES sejour (id_sejour)
)
;

----- Création de la table acte ------
-----------------------------------------

CREATE TABLE acte(
--
id_acte SERIAL NOT NULL,
libelle_acte VARCHAR(50) NOT NULL,
code_acte VARCHAR(50) NOT NULL,
date_debut_validite DATE NOT NULL,
date_fin_validite DATE NOT NULL,
--
PRIMARY KEY (id_acte)
)
;

----- Création de la table rum_acte ------
--------------------------------------------

CREATE TABLE rum_acte (
--
id_rum INT NOT NULL,
id_acte INT NOT NULL,
date_acte DATE NOT NULL,
--
PRIMARY KEY (id_rum, id_acte, date_acte),
FOREIGN KEY (id_rum) REFERENCES rum (id_rum),
FOREIGN KEY (id_acte) REFERENCES acte (id_acte)
)
;


----- Création de la table diagnostic ------
----------------------------------------------

CREATE TABLE diagnostic(
--
id_diagnostic SERIAL NOT NULL,
libelle_diagnostic VARCHAR(50) NOT NULL,
code_diagnostic VARCHAR(50) NOT NULL,
date_debut_validite DATE NOT NULL,
date_fin_validite DATE NOT NULL,
--
PRIMARY KEY (id_diagnostic)
)
;

----- Création de la table rum_diagnostic ------
-------------------------------------------------

CREATE TABLE rum_diagnostic (
--
id_rum INT NOT NULL,
id_diagnostic INT NOT NULL,
date_diagnostic DATE NOT NULL,
--
PRIMARY KEY (id_rum, id_diagnostic, date_diagnostic),
FOREIGN KEY (id_rum) REFERENCES rum (id_rum),
FOREIGN KEY (id_diagnostic) REFERENCES diagnostic (id_diagnostic)
)
;