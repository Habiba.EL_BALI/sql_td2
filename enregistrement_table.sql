-- Script SQL de création d'enregistrements pour les tables:

-- rum,rum_acte,rum_diagnostic

-----------------------------------------------
-----------------------------------------------

----- Enregistrements de la table rum ------
--------------------------------------------

INSERT INTO rum 
(id_sejour, date_entree, date_sortie, mode_admission, mode_sortie)
VALUES (1,'2020-01-01', '2020-01-10', 'Hospitalisation programmee','HAD'),
(2,'2020-01-01','2020-01-12', 'Urgence','SL'),
(3,'2020-01-20','2020-01-25', 'Hospitalisation programmee','HAD'),
(4,'2020-01-01', '2020-01-08','Urgence','HAD'),
(5,'2020-02-02','2020-02-04','Hospitalisation programmee','SL');


----- Enregistrements de la table rum_acte ------
-------------------------------------------------

INSERT INTO rum_acte
(id_rum, id_acte, date_acte)
VALUES (1,1,'2020-01-02'),
(2,2,'2020-01-05'),
(3,3,'2020-01-22'),
(4,4,'2020-01-05'),
(5,5,'2020-02-03');


----- Enregistrements de la table rum_diagnostic ------
-------------------------------------------------------

INSERT INTO rum_diagnostic
(id_rum, id_diagnostic, date_diagnostic)
VALUES (1,1,'2020-01-01'),
(2,2,'2020-01-04'),
(3,3,'2020-01-20'),
(4,4,'2020-01-04'),
(5,5,'2020-02-02');
