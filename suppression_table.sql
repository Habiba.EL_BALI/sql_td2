
-- Script SQL de supression des tables :

-- rum,rum_acte,acte,rum_diagnostic,diagnostic

-----------------------------------------------
-----------------------------------------------

----- Supression de la table rum_acte ------
--------------------------------------------

DROP TABLE rum_acte;


----- Supression de la table rum_diagnostic ------
-------------------------------------------------

DROP TABLE rum_diagnostic;


----- Supression de la table rum ------
-----------------------------------------

DROP TABLE rum;

----- Supression de la table acte ------
-----------------------------------------

DROP TABLE acte;


----- Supression de la table diagnostic ------
----------------------------------------------

DROP TABLE diagnostic;
